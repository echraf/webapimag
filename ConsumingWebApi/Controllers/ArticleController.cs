﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace ConsumingWebApi.Controllers
{
    public class ProductController : Controller
    {
        public ActionResult GetAllArticles()
        {
            try
            {
                ServiceRepository serviceObj = new ServiceRepository();
                HttpResponseMessage response = serviceObj.GetResponse("api/showroom/getallArticles");
                response.EnsureSuccessStatusCode();
                List<Models.Article> Articles = response.Content.ReadAsAsync<List<Models.Article>>().Result;
                ViewBag.Title = "All Articles";
                return View(Articles);
            }
            catch (Exception)
            {
                throw;
            }
        }
        //[HttpGet]  
        public ActionResult EditProduct(int id)
        {
            ServiceRepository serviceObj = new ServiceRepository();
            HttpResponseMessage response = serviceObj.GetResponse("api/showroom/GetArticle?id=" + id.ToString());
            response.EnsureSuccessStatusCode();
            Models.Article Articles = response.Content.ReadAsAsync<Models.Article>().Result;
            ViewBag.Title = "All Articles";
            return View(Articles);
        }
        //[HttpPost]  
        public ActionResult Update(Models.Article article)
        {
            ServiceRepository serviceObj = new ServiceRepository();
            HttpResponseMessage response = serviceObj.PutResponse("api/showroom/UpdateProduct", article);
            response.EnsureSuccessStatusCode();
            return RedirectToAction("GetAllArticle");
        }
        public ActionResult Details(int id)
        {
            ServiceRepository serviceObj = new ServiceRepository();
            HttpResponseMessage response = serviceObj.GetResponse("api/showroom/GetArticle?id=" + id.ToString());
            response.EnsureSuccessStatusCode();
            Models.Article Articles = response.Content.ReadAsAsync<Models.Article>().Result;
            ViewBag.Title = "All Articles";
            return View(Articles);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Models.Article Article)
        {
            ServiceRepository serviceObj = new ServiceRepository();
            HttpResponseMessage response = serviceObj.PostResponse("api/showroom/InsertProduct", Article);
            response.EnsureSuccessStatusCode();
            return RedirectToAction("GetAllArticles");
        }
        public ActionResult Delete(int id)
        {
            ServiceRepository serviceObj = new ServiceRepository();
            HttpResponseMessage response = serviceObj.DeleteResponse("api/showroom/DeleteArticle?id=" + id.ToString());
            response.EnsureSuccessStatusCode();
            return RedirectToAction("GetAllArticles");
        }
    }
}