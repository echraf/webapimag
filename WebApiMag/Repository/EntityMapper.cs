﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiMag.Models;


namespace WebApiMag.Repository
{
    public class EntityMapper<TSource, TDestination> where TSource : class where TDestination : class
    {
        public EntityMapper()
        {
            var config1 = new MapperConfiguration(cfg => {
                cfg.CreateMap<Models.Article, Article>();
            });
            var config2 = new MapperConfiguration(cfg => {
                cfg.CreateMap<Article, Models.Article>();
            });

            IMapper mapper1 = config1.CreateMapper();
            IMapper mapper2 = config2.CreateMapper();


        }
        public TDestination Translate(TSource obj)
        {
            return Mapper.Map<TDestination>(obj);
        }
    }
}