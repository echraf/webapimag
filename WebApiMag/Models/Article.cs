﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiMag.Models
{
    public class Article
    {
        public int Id_Art { get; set; }
        public string CODART { get; set; }
        public string CODCAT { get; set; }
        public string departement { get; set; }
        public string rayon { get; set; }
        public string Famille { get; set; }
        public string sous_famille { get; set; }
        public string DESART { get; set; }
        public string REFART { get; set; }
        public string MARQART { get; set; }
        public string fournisseur { get; set; }
        public Nullable<double> PMP_FINAL { get; set; }
        public Nullable<float> tva { get; set; }
        public Nullable<float> marge { get; set; }
        public Nullable<double> PVHT { get; set; }
        public Nullable<double> PVTTC { get; set; }
        public string UNITE { get; set; }
        public string OBS { get; set; }
        public Nullable<int> min { get; set; }
        public Nullable<int> max { get; set; }
        public string Action_achat { get; set; }
        public string Action_vente { get; set; }
    }
}