﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using AutoMapper;
using WebApiMag.Repository;
using APIMltMag;
    namespace WebApiMag.Controllers
{
    public class ShowroomController : ApiController
    {
        [HttpGet]
        public JsonResult<List<Models.Article>> GetAllArticles()
        {
            EntityMapper<APIMltMag.ARTICLE, Models.Article> mapObj = new EntityMapper<APIMltMag.ARTICLE, Models.Article>();
            List<APIMltMag.ARTICLE> ArtList = APIMltMag.DAL.GetAllArticles();
            List<Models.Article> Articles = new List<Models.Article>();
            foreach (var item in ArtList)
            {
                Articles.Add(mapObj.Translate(item));
            }
            return Json<List<Models.Article>>(Articles);
        }
        [HttpGet]
        public JsonResult<Models.Article> GetArticle(int id)
        {
            EntityMapper<APIMltMag.ARTICLE, Models.Article> mapObj = new EntityMapper<APIMltMag.ARTICLE, Models.Article>();
            APIMltMag.ARTICLE dalArticle = DAL.GetArticle(id);
            Models.Article Articles = new Models.Article();
            Articles = mapObj.Translate(dalArticle);
            return Json<Models.Article>(Articles);
        }
        [HttpPost]
        public bool InsertArticle(Models.Article article)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                EntityMapper<Models.Article, APIMltMag.ARTICLE> mapObj = new EntityMapper<Models.Article, APIMltMag.ARTICLE>();
                APIMltMag.ARTICLE ArtObj = new APIMltMag.ARTICLE();
                ArtObj = mapObj.Translate(article);
                status = DAL.InsertArticle(ArtObj);
            }
            return status;

        }
       

    }
}