﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace APIMltMag
{
    public class DAL
    {
        static DATAMag DbContext;
        static DAL ()
        {
            DbContext = new DATAMag();
        }
          public static List<ARTICLE> GetAllArticles()
        {
            return DbContext.ARTICLE.ToList();
        }
         public static ARTICLE GetArticle(int IdArt)
        {
            return DbContext.ARTICLE.Where(A => A.Id_Art == IdArt).FirstOrDefault();
        }
        public static bool InsertArticle(ARTICLE art)
        {
            bool statut ;
            try
        {
                DbContext.ARTICLE.Add(art);
                DbContext.SaveChanges();
                statut = true;
            }
            catch
        {
                statut = false;
            }
            return statut;
        }
    }
 
}
